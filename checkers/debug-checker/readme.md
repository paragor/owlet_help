Просто стандартный пример для checker`а

возвращает если в файле debug=true
```json
{
  "isChange": true,
  "newTag": "awesometag",
  "Actions": [
    {
      "sender": "default",
      "content": "CO CO CO content"
    }
  ]
}
```

если debug=false, возвращает
```json
{"isChange": false}
```

