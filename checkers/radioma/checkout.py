#!/usr/bin/env python3

import json
import sys
import xml.etree.ElementTree as ElementTree

import urllib.request


"""
Проверяет новые выпуски подкаста [Радиома](http://radioma.org/)



Принимает на вход json объект
```json
{
    "lastTag":"Радиома 000"
}

```

Если вышел новый выпуск, то возвращает объект:

```json
{
    "isChange": true,
    "newTag": "Радиома 002",
    "Actions": [
        {
            "sender": "telegram",
            "content": "Вышел новый подкаст! Радиома 001 http://ссылка/на/подкаст"
        }
    ]
}
```
если нового выпуска не было:
```json
{
    "isChange": false
}
```
"""

if len(sys.argv) != 2:
    print("проблема с аргументами:" + sys.argv.__str__(), file=sys.stderr)
    exit(1)

input_params = json.loads(sys.argv[1])
old_title = input_params['lastTag']

# make request
url = 'http://feeds.feedburner.com/It-Radioma?format=xml'

req = urllib.request.Request(url)
webURL = urllib.request.urlopen(req)
encoding = webURL.info().get_content_charset('utf-8')
response = (webURL.read()).decode()



# parse response
root = ElementTree.fromstring(response)
latest_release = root[0].find('item')
title = latest_release.find('title').text
link = latest_release.find('link').text

if title == old_title:
    response = {
        "isChange": False,
    }
else:
    response = {
        "isChange": True,
        "newTag": title,
        "Actions": [
            {
                "sender": "telegram",
                "content": "Вышел новый подкаст! " + title + " " + link
            },
            {
                "sender": "default",
                "content": "Вышел новый подкаст! " + title + " " + link
            }
        ]
    }

print(json.dumps(response, ensure_ascii=False))
