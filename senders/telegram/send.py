#!/usr/bin/python3

import configparser
import inspect
import os
import sys
import re
import json

import urllib.request
import urllib.parse

"""
Скрипт позволяет посылать через бота сообщения себе в личку


Принимает в качестве первого аргумента(argv[1]) строку:
"Сообщение, которое нужно послать"

Необходимо скоприровать sample.config.ini -> config.ini

в config файле берет "chat_id" пользователя и "token" бота. Чтобы получить эти параметры необходимо:
* Зарегистрировать бота можно через @BotFather (занимает 2 минуты).
прежде чем использовать бота, необходимо ему написать!
* chat_id для лички можно узнать, написав боту @ShowJsonBot
в ответе[message][from][id] будет нужный id
"""


def get_script_dir(follow_symlinks=True):
    if getattr(sys, 'frozen', False):
        path = os.path.abspath(sys.executable)
    else:
        path = inspect.getabsfile(get_script_dir)
    if follow_symlinks:
        path = os.path.realpath(path)
    return os.path.dirname(path)


config = configparser.ConfigParser()
config.read(get_script_dir() + "/config.ini")

token = config['telegram']['token']
chat_id = config['telegram']['chat_id']

if len(sys.argv) != 2:
    print("проблема с аргументами:" + sys.argv.__str__(), file=sys.stderr)
    exit(1)

msg_to_send = sys.argv[1]
CDATA_pattern = re.compile("^<!\[CDATA\[([\s\S]+)\]\]$")
if CDATA_pattern.match(msg_to_send):
    msg_to_send = msg_to_send[9:-2]


# make request
method = 'sendMessage'
url = 'https://api.telegram.org/bot' + token + '/' + method
data = urllib.parse.urlencode({"chat_id": chat_id, "text": msg_to_send}).encode("utf-8")
req = urllib.request.Request(url)
webURL = urllib.request.urlopen(req, data=data)
resp = webURL.read()
encoding = webURL.info().get_content_charset('utf-8')
r_json = json.loads(resp.decode(encoding))

if r_json['ok']:
    exit(0)
else:
    print(r_json, file=sys.stderr)
    exit(1)
